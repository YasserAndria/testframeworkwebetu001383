ANDRIAMIARISOA Yasser P14A12 ETU001383
Ceci est le projet de test du projet FrameworkWeb présent sur le lien : https://gitlab.com/YasserAndria/genericfrontservlet
ce projet est l'extrait d'un projet java web qui a été developper sur l'IDE netbeans et est composé de 3 dossiers:
    src : contient le code source java utilisé dans ce projet, elle contient notament lé class model.personne, créez afin de tester le framework en utilisant les annotations et les mapping de celui-ci
    web : contient la structure web du projet ainsi que les page jsp et html du projets mais surtout le fichier web.xml où a été faite les configurations nécéssaire au fonctionnement du framewrok(Comme indiqué dans le fichier readme.txt de celui-ci).
    build : contient le projet web build-é par l'ide.Elle contient notament la structure web comme dans le dossier web, mais aussi les fichier .class compiler depuis les codes java de src.
            mais surtout, build contient dans build\web\WEB-INF\lib le fichier FrameWorkWebSrc.jar compiler depuis le projet frameworkWeb que nous souhaitons Tester ici.
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import frameworkweb.ModelView;
import frameworkweb.annotations.Url;
import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author lenovo
 */
public class Personne {
    private String nom;
    private String prenom;
    private Date dateN;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateN() {
        return dateN;
    }

    public void setDateN(Date dateN) {
        this.dateN = dateN;
    }
    @Url(name = "insertpers")
    public ModelView create(){
        ModelView result = new ModelView();
        result.setUrlRedirect("ConfirmationInsPers.jsp");
        String d = "la personne du nom de:"+getNom()+" "+getPrenom()+" née le: "+getDateN().toString()+" à bien été inserer dans le systeme";
        result.getData().put("confirmation",d);
        return result;
    }
    @Url(name = "listpers")
    public ModelView lister(){
        ModelView result = new ModelView();
        result.setUrlRedirect("ListPers.jsp");
        ArrayList<Personne> listpers = new ArrayList<>();
        listpers.add(new Personne("Rakoto","Be",Date.valueOf("2000-08-21")));
        listpers.add(new Personne("Rakoto","fra",Date.valueOf("2001-12-01")));
        listpers.add(new Personne("Rabe","Jean",Date.valueOf("2003-01-09")));
        listpers.add(new Personne("Rasoa","Tiana",Date.valueOf("2000-10-13")));
        result.getData().put("liste", listpers);
        return result;
    }
    @Url(name = "acceuilpers")
    public ModelView acceuil(){
        ModelView result = new ModelView();
        result.setUrlRedirect("FormulaireInsertPersonne.jsp");
        return result;
    }

    public Personne(String nom, String prenom, Date dateN) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateN = dateN;
    }

    public Personne() {
    }
}

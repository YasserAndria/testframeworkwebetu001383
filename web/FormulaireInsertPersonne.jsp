<%-- 
    Document   : Formulaire
    Created on : 13 nov. 2022, 12:32:27
    Author     : lenovo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Formulaire de test</title>
    </head>
    <body>
        <h1>Formulaire de test</h1>
        <form action="http://localhost:8080/TestFW/insertpers.do" method="post">
            <div class="form-group">
                <label for="nom">Nom</label>
                <input type="text" name="nom" class="form-control" id="nom" placeholder="nom ici">
            </div>
            <div class="form-group">
                <label for="prenom">Prénom</label>
                <input type="text" name="prenom" class="form-control" id="prenom" placeholder="prenom ici">
            </div>
            <div class="form-group">
                <label for="dateN">date de Naissance</label>
                <input type="date" name="dateN" class="form-control" id="dateN">
            </div>
            <button type="submit" style="margin-top: 0.25em" class="btn btn-success btn-rounded">Ajouter</button>

        </form>
        <a href="http://localhost:8080/TestFW/listpers.do">Liste des personnes</a>
    </body>
</html>

<%-- 
    Document   : ListPers
    Created on : 13 nov. 2022, 14:22:25
    Author     : lenovo
--%>

<%@page import="model.Personne"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Liste Personne</title>
    </head>
    <body>
        <h1>Liste Personne</h1>
        <%
            ArrayList<Personne> liste = (ArrayList<Personne>)request.getAttribute("liste");
            for (Personne elem : liste ) {
                String mess="la personne du nom de :"+elem.getNom()+" "+elem.getPrenom()+" née le: "+elem.getDateN().toString()+" est présent dans le systeme" ;
                out.println(mess);
                out.println("<br/>");
            }
        %>
    </body>
</html>
